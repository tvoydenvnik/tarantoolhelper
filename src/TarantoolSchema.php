<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 18.02.2016
 * Time: 21:33
 */

namespace Tvoydenvnik\TarantoolHelper;


class TarantoolSchema {


    /**
     * @var $connection \Tarantool
     */
    private  $connection = null;

    /**
     * @param $connection \Tarantool
     */
    public function setConnection($connection){
        $this->connection = $connection;
    }

    /**
     * @var $lastError \Exception
     */
    public $lastError;

     /**
     * Create a space.
     * http://tarantool.org/doc/book/box/box_schema.html
     * box.schema.space.create(space-name[, {options}])
     * @param $sSpaceName
     * @param array $arOptions
     * @return bool|number
     */
    public function spaceCreate($sSpaceName, $arOptions = array()){


        $sEval = "box.schema.space.create('".$sSpaceName."', ".self::arrayToLuaTable($arOptions).")" . "\n"
            . " return box.space.".$sSpaceName.".id";
        $lResult = $this->_evaluateWithReturn($sEval);
        if($lResult !== false){
            return $lResult[0];
        }
        return false;

    }

    /**
     * Create a user.
     * box.schema.user.create(user-name[, {options}])
     * @param $sUserName
     * @param array $arOptions
     * @return bool
     */
    public function userCreate($sUserName, $arOptions = array()){

        $sEval = "box.schema.user.create('".$sUserName."', ".self::arrayToLuaTable($arOptions).")";
        return $this->_evaluate($sEval);

    }

    /**
     * Drop a user.
     * box.schema.user.drop(user-name[, {options}])
     * @param $sUserName
     * @param array $arOptions
     * @return bool
     */
    public function userDrop($sUserName, $arOptions = array()){

        $sEval = "box.schema.user.drop('".$sUserName."', ".self::arrayToLuaTable($arOptions).")";
        return $this->_evaluate($sEval);

    }

    /**
     * Return true if a user exists; return false if a user does not exist.
     * box.schema.user.exists(user-name)
     * @param $sUserName
     * @return bool
     */
    public function userExists($sUserName){

        $sEval = "box.schema.user.exists('".$sUserName."')";
        return $this->_evaluate($sEval);

    }

    //todo box.schema.user.grant(user-name, privileges)
//    public function userGrant($sUserName, $arPrivileges = array()){
//
//        $sEval = "box.schema.user.grant('".$sUserName."', ".self::arrayToLuaTable($arPrivileges).")";
//        return $this->_evaluate($sEval);
//
//    }

//todo  box.schema.user.revoke(user-name, privileges)

    private function _evaluate($sEval){

        try{

            $this->connection->evaluate($sEval);
            return true;

        }catch (\Exception $e) {

            $this->lastError = $e;
            return false;

        }
    }

    //todo box.schema.user.password(password)

    //todo box.schema.user.passwd([user-name, ]password)

    //todo box.schema.user.info([user-name])

    //todo box.schema.role.create(role-name[, {options}])

    //todo box.schema.role.drop(role-name)

    //todo box.schema.role.exists(role-name)

    //todo box.schema.role.grant(role-name, privileges)

    //todo box.schema.role.revoke(role-name, privileges)

    //todo box.schema.role.info([role-name])

    //todo box.schema.func.create(func-name[, {options}])

    //todo box.schema.func.drop(func-name)

    //todo box.schema.func.exists(func-name)

    private function _evaluateWithReturn($sEval){

        try{

            return $this->connection->evaluate($sEval);

        }catch (\Exception $e) {

            $this->lastError = $e;
            return false;

        }
    }

    public static function arrayToLuaTable($arArray){

        if(count($arArray)>0){
            return '{}';
        }
        $tmpArOptions = array();
        foreach($arArray as $key=>$value){

            if(is_string($value)){

                array_push($tmpArOptions, $key . ' = \'' . $arArray[$key]."'");

            }elseif(is_bool($value)){

                array_push($tmpArOptions, $key . ' = ' . ($arArray[$key]===true?'true':'false'));

            }elseif(is_int($value)) {

                array_push($tmpArOptions, $key . ' = ' . $arArray[$key]);

            }elseif(is_null($value)){

                array_push($tmpArOptions, $key . ' = nil ');

            }
        }

        $sOptions = implode(',', $tmpArOptions);

        return '{' . $sOptions . '}';
    }
}