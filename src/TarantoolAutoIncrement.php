<?php

namespace Tvoydenvnik\TarantoolHelper;


class TarantoolAutoIncrement {


    private static $cSPACE_AUTO_INCREMENT = '_auto_increment_';

    public static function emptyAutoIncrement(\Tarantool $connection, $sSpaceName){
        $connection->delete(self::$cSPACE_AUTO_INCREMENT, array($sSpaceName));
    }

    public static function getNewId(\Tarantool $connection, $sSpaceName){

        try{
            $arChanel = $connection->select(self::$cSPACE_AUTO_INCREMENT, array($sSpaceName), "primary");
        }catch (\Exception $e){
            self::initSchema($connection);
            $arChanel = $connection->select(self::$cSPACE_AUTO_INCREMENT, array($sSpaceName), "primary");
        }

        $newId = 1;
        if(count($arChanel) === 0){

            $connection->insert(self::$cSPACE_AUTO_INCREMENT,array($sSpaceName, 1));

        }else{

            $arResult = $connection->update(self::$cSPACE_AUTO_INCREMENT,array($sSpaceName), array(array(
                "field" => 1,
                "op" => "+",
                "arg" => 1
            )), "primary");

            $newId = $arResult[0][1];
        }

        return $newId;
    }


    public static function initSchema(\Tarantool $connection, $sUserName = 'app'){

        TarantoolHelper::createSpace($connection, self::$cSPACE_AUTO_INCREMENT, array('user'=>$sUserName, 'if_not_exists'=>true));
        TarantoolHelper::createIndex($connection, self::$cSPACE_AUTO_INCREMENT , 'primary', 'hash', true, array(1, 'STR'), true);

    }
}